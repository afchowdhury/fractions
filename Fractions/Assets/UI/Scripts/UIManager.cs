﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject UtilsPanel;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject instructPanel;
    [SerializeField] private GameObject endPanel;
    
    [Header("Setup")]
    [SerializeField] private bool ShowInstructionsOnStartup = true;

    // Start is called before the first frame update
    void Start()
    {
        #if !UNITY_WEBGL
            UtilsPanel.SetActive(false);
        #endif
        
        pauseButton.SetActive(!ShowInstructionsOnStartup);
        InstructionEnable(ShowInstructionsOnStartup);
    }
    
    // This is a change to demonstrate a change.
    // Second change.

    public void Pause (){
        pauseButton.SetActive(false);
        pausePanel.SetActive(true);
    }

    public void UnPause (){
        pauseButton.SetActive(true);
        pausePanel.SetActive(false);
    }

    public void InstructionEnable(bool fromPause){
        
        instructPanel.SetActive(true);
        if(fromPause == true){
        pausePanel.SetActive(false);
        }
    }

    public void CloseInstructions(){
        instructPanel.SetActive(false);
        pauseButton.SetActive(true);
    }

    public void EndScreen (){
        endPanel.SetActive(true);
        pauseButton.SetActive(false);
    }

}
