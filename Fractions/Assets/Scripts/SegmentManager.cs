﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SegmentManager : MonoBehaviour
{
    public Button button;
    public GameObject container;
    public int segmentAmount, maxSegments, yellowCount, greenCount;
    private bool isYellow;
    public GameObject fraction, ratio, decimalDisplay, percentage;
    public Text numerator, denominator;
    public Text yellowRatio, greenRatio;
    public float decimalFloat;
    public float percentageDisplay;
    public List <GameObject> zeChildren = new List <GameObject>();
    public ColourChange startingKid;
    public bool textVisible;
    public int buttonLoop;
    public Text buttonText;
    
    public void Start()
    {
        Setup();

        //buttonLoop = 0;

        //SwitchSolution();

        // segmentAmount = 1;
        // greenCount = 1;
        // yellowCount = 0;

        // buttonLoop = 1;
        // SwitchSolution();
        // textVisible = true;

        // ShowFraction();
        // ShowRatio();
        // ShowDecimal();
        // ShowPercentage();
    }

    public void Setup()
    {
        segmentAmount = 1;
        greenCount = 1;
        yellowCount = 0;

        textVisible = true;
        buttonLoop = 1;
        SwitchSolution();
    

        ShowFraction();
        ShowRatio();
        ShowDecimal();
        ShowPercentage();
    }

    public void DestroyTheChildren()
    {

        
            for (int i = 0; i < zeChildren.Count; i++)
            {
                Destroy(zeChildren[i]);
            }  
           
        startingKid.activated = true;
        startingKid.ColorSwitch();

    }
    
    public void ButtonPress()
    {
        if (segmentAmount < maxSegments)
        {

        Button clone = Instantiate(button,container.transform);
        zeChildren.Add (clone.gameObject);
        segmentAmount ++;
        greenCount ++;

        }
        ShowFraction();
        ShowRatio();
        ShowDecimal();
        ShowPercentage();

    }

    public void subtractSegment()
    {
        if (segmentAmount > 1)
        {
            isYellow = container.GetComponent<Transform>().GetChild(segmentAmount - 1).gameObject.GetComponent<ColourChange>().activated;
            if(!isYellow)
            {
                Destroy(container.GetComponent<Transform>().GetChild(segmentAmount - 1).gameObject);
                segmentAmount --;
                greenCount --;

            }
            else
            {
                Destroy(container.GetComponent<Transform>().GetChild(segmentAmount - 1).gameObject);
                segmentAmount --;
                yellowCount --;

            }

            ShowFraction();
            ShowRatio();
            ShowDecimal();
            ShowPercentage();
        }
    }

    public void Update()
    {
        //colourChangesList.GetComponent<transform>();

         if(Input.GetKeyDown(KeyCode.F)){
            for (int i = 0; i < zeChildren.Count; i++)
            {
                Destroy(zeChildren[i]);
            }  
        }

    }

    public void ShowFraction()
    {
        numerator.text = yellowCount.ToString();
        denominator.text = segmentAmount.ToString();
    }

    public void ShowRatio()
    {
        yellowRatio.text = yellowCount.ToString();
        greenRatio.text = greenCount.ToString();

    }

    public void ShowDecimal()
    {
        decimalFloat = ((float)yellowCount/(float)segmentAmount);
        decimalDisplay.GetComponent<Text>().text = decimalFloat.ToString("F3");

    }

    public void ShowPercentage()
    {
        percentageDisplay = (((float)yellowCount/(float)segmentAmount)*100);
        percentage.GetComponent<Text>().text = (percentageDisplay.ToString("F1") + "%");

    }
  
    public void SwitchSolution()
    {
        if(textVisible)
        {
            switch(buttonLoop)
            {
                case 1:
                    buttonLoop++;
                    fraction.SetActive(true);
                    percentage.SetActive(false);
                    Debug.Log("working1");

                break;

                case 2:
                    buttonLoop++;
                    ratio.SetActive(true);
                    fraction.SetActive(false);
                    Debug.Log("working2");
                break;

                case 3:
                    buttonLoop++;
                    decimalDisplay.SetActive(true);
                    ratio.SetActive(false);
                    Debug.Log("working3");
                break;

                case 4:
                    buttonLoop = 1;
                    percentage.SetActive(true);
                    decimalDisplay.SetActive(false);
                    Debug.Log("working4");
                break;
            }
        }

    }

    public void HideAnswer()
    {
        if(textVisible)
        {
            ratio.SetActive(false);

            fraction.SetActive(false);

            decimalDisplay.SetActive(false);

            percentage.SetActive(false);

            textVisible = false;

            buttonText.text = "Show Answers";

        }
        
        else
        {
            textVisible = true;
            
            if(buttonLoop == 1)
            {
                buttonLoop = 4;
            }
            else
            {
                buttonLoop --;
            }
            SwitchSolution();

            buttonText.text = "Hide Answers";
            
        }

        
        
        // ratio.SetActive(false);
        // decimalDisplay.SetActive(false);
        // fraction.SetActive(false);
        // percentage.SetActive(true);
    }

    

    
}
