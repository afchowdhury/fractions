﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetButton : MonoBehaviour
{
    public SegmentManager[] segmentManagers;

    public SegmentManager segmentManager;

    //public int segmentAmount, yellowCount, greenCount;


    public void ResetIt(){
        foreach (var item in segmentManagers)
        {
            item.DestroyTheChildren();
            item.Setup();
        }

        

        //ResetCalculations();

        //segmentManager.Start();

        // segmentManager.ShowFraction();
        // segmentManager.ShowRatio();
        // segmentManager.ShowDecimal();
        // segmentManager.ShowPercentage();
    }

    // public void ResetCalculations()
    // {
    //     segmentManager.segmentAmount = 1;
    //     segmentManager.greenCount = 1;
    //     segmentManager.yellowCount = 0;

    //     segmentManager.numerator.text = segmentManager.yellowCount.ToString();
    //     segmentManager.denominator.text = segmentManager.segmentAmount.ToString();

    //     segmentManager.yellowRatio.text = segmentManager.yellowCount.ToString();
    //     segmentManager.greenRatio.text = segmentManager.greenCount.ToString();

        // numerator.text = yellowCount.ToString();
        // denominator.text = segmentAmount.ToString();

        // yellowRatio.text = yellowCount.ToString();
        // greenRatio.text = greenCount.ToString();

        // decimalFloat = 0;
        // decimalDisplay.GetComponent<Text>().text = decimalFloat.ToString("F3");

        // percentageDisplay = 0;
        // percentage.GetComponent<Text>().text = (percentageDisplay.ToString("F1") + "%");

    //}

}
