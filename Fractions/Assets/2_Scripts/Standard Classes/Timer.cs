using System;

namespace TwinklStandardClasses
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    
    
    public class Timer : MonoBehaviour
    {
        public enum UpdateInterval
        {
            seconds,
            minSec,
            minSecMS
        }
        
        public float timeRemaining = 0f;
        public bool timerIsRunning = false;
        
        public UpdateInterval accuracy;

        private Action<string> OnTimeUpdated;
        public Action OnTimerCompleted;

        public void CancelTimer()
        {
            timeRemaining = 0;
            timerIsRunning = false;
        }

        public void SetTimer(float _seconds)
        {
            timeRemaining = _seconds;
        }

        public void PauseTimer()
        {
            timerIsRunning = false;
        }

        public void ContinueTimer()
        {
            timerIsRunning = true;
        }

        private float timeSinceLastUpdate;
        void FixedUpdate()
        {
            if (timerIsRunning)
            {
                if (timeRemaining > 0)
                {
                    timeRemaining -= Time.deltaTime;
                    timeSinceLastUpdate += Time.deltaTime; 
                    CheckForUpdate();
                }
                else
                {
                    timeRemaining = 0;
                    timerIsRunning = false;
                    OnTimerCompleted.Invoke();
                }
            }
        }

        public void CheckForUpdate()
        {
            if(OnTimeUpdated == null)
                return;
            
            float _timeToDisplay = timeRemaining + 1;
            string _formattedTime = "";
            if (accuracy == UpdateInterval.minSecMS)
            {
                _formattedTime = FormatStringToMilliseconds(_timeToDisplay);
            }
            else if (timeSinceLastUpdate >= 1f)
            {
                _formattedTime = accuracy == UpdateInterval.seconds
                    ? FormatStringToSeconds(_timeToDisplay)
                    : FormatStringToMinutes(_timeToDisplay);
            }
            else
            {
                return;
            }
            OnTimeUpdated.Invoke(_formattedTime);
            timeSinceLastUpdate = 0f;
        }

        private static string FormatStringToSeconds(float _timeInSeconds)
        {
            float minutes = Mathf.FloorToInt(_timeInSeconds / 60); 
            float seconds = Mathf.FloorToInt(_timeInSeconds % 60);
            return string.Format("{0:00}:{1:00}", minutes, seconds);
        }

        private static string FormatStringToMinutes(float _timeInSeconds)
        {
            return "";
        }

        private static string FormatStringToMilliseconds(float _timeInSeconds)
        {
            return "";
        }
    }
}