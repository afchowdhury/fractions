﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TwinklAuthenticationGateway;
using TwinklStandardClasses;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TwinklGameManager
{
    /// <summary>
    /// Controls transition between Unity scenes.
    /// Various options for scene loading.
    /// </summary>
    public class SceneController: Singleton<SceneController>
    {
        List<string> protectedScenes = new List<string>();
        List<string> loadedScene = new List<string>();

        private bool unloadSceneOnLoad = false;

        protected override void Awake()
        {
            loadedScene.Add(SceneManager.GetActiveScene().name);
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += onSceneUnloaded;
        }
            
        public void SwitchScene(string _sceneName)
        {
            unloadSceneOnLoad = true;
            if (!loadedScene.Contains(_sceneName))
            {
                SceneManager.LoadScene(_sceneName, LoadSceneMode.Additive);
            }
            else
            {
                Debug.LogWarningFormat("Scene {0} is already loaded", _sceneName);
            }
        }

        public void TransitionToScene(iTransitionPanel _panel)
        {
            _panel.UncoverScreen(()=> SwitchScene("Game"));
        }
        
        public void AddScene(string _sceneName)
        {
            unloadSceneOnLoad = false;
            if (!SceneManager.GetSceneByName(_sceneName).isLoaded)
            {
                SceneManager.LoadScene(_sceneName, LoadSceneMode.Additive);
            }
        }
        
        public void AddPersistentScene(string _sceneName)
        {
            protectedScenes.Add(_sceneName);
            if (!SceneManager.GetSceneByName(_sceneName).isLoaded)
            {
                AddScene(_sceneName);
            }
        }
        
        public void ForceUnloadScene(string _sceneName)
        {
            if (protectedScenes.Contains(_sceneName))
                protectedScenes.Remove(_sceneName);
            SceneManager.UnloadSceneAsync(_sceneName);
        }

        public void ClearAllScenes(string _sceneName)
        {
            protectedScenes = new List<string>();
            if (!SceneManager.GetSceneByName(_sceneName).isLoaded)
            {
                SceneManager.LoadScene(_sceneName, LoadSceneMode.Additive);
            }
            else
            {
                
                UnloadAllUnprotectedScenes();
            }

        }

        private void UnloadAllUnprotectedScenes()
        {
            foreach (string _loadedSceneName in loadedScene.Except(protectedScenes))
            {
                SceneManager.UnloadSceneAsync(_loadedSceneName);
            }
        }
        
        private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
        {
            if (unloadSceneOnLoad)
            {
                UnloadAllUnprotectedScenes();
            }
        }
        
        private void onSceneUnloaded(Scene _scene)
        {
            if (protectedScenes.Contains(_scene.name))
            {
                Debug.LogWarningFormat("Removed protected scene from list {0}", _scene.name);
                protectedScenes.Remove(_scene.name);
            }

            if (loadedScene.Contains(_scene.name))
            {
                loadedScene.Remove(_scene.name);
            }
        }
    }
}