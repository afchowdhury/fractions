using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{

    public GameObject outerNumbersParent;
    public GameObject innerNumbersParent;
    public GameObject middleNumber;

    public TMP_Text[] innerNumbersText;
    public TMP_Text[] outerNumbersText;
    public TMP_Text middleNumberText;

    public TMP_Text toggleInnerNumbersButtonText;
    public TMP_Text toggleOuterNumbersButtonText;
    public TMP_Text toggleMiddleNumberButtonText;

    private int activeMiddleNumber = 10;

    private int[] randomNumbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
    private int[] randomInnerNumbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    //Set up inner numbers as list/array for each random middle number -> private int[] innerNumbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
    //Outer numbers same? -> private int[] outerNumbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100 };



    // Shuffle randomNumbers Array
    private void ShuffleArray<T>(T[] arr)
    {
        for (var i = arr.Length - 1; i > 0; i--)
        {
            var r = Random.Range(0, i + 1);
            T tmp = arr[i];
            arr[i] = arr[r];
            arr[r] = tmp;
        }
    }


    // Button Functions
    public void OuterNumbersButtonToggle()
    {
        ToggleTextButton(!outerNumbersParent.activeSelf, outerNumbersParent, toggleOuterNumbersButtonText, " Outer Numbers");
    }
    
    public void InnerNumbersButtonToggle()
    {
        ToggleTextButton(!innerNumbersParent.activeSelf, innerNumbersParent, toggleInnerNumbersButtonText, " Inner Numbers");
    }
    
    public void MiddleNumberButtonToggle()
    {
        ToggleTextButton(!middleNumber.activeSelf, middleNumber, toggleMiddleNumberButtonText, " Middle Number");
    }

    private void ToggleTextButton(bool isActive, GameObject button, TMP_Text buttonText, string suffix)
    {
        button.SetActive(isActive);

        string text;
        if (button.activeSelf)
        {
            text = "Hide";
        }
        else
        {
            text = "Show";
        }
        buttonText.text = text + suffix;
    }

    private void UpdateNumbers()
    {
        middleNumberText.text = activeMiddleNumber.ToString();

        for (int i = 0; i < innerNumbersText.Length; i++)
        {
            innerNumbersText[i].text = randomInnerNumbers[i].ToString();
            outerNumbersText[i].text = (randomInnerNumbers[i] * activeMiddleNumber).ToString();
        }
    }

    public void RandomiseDial()
    {
        activeMiddleNumber = GetRandomMiddleNumber();
        ShuffleArray(randomInnerNumbers);

        UpdateNumbers();

        ToggleTextButton(false, outerNumbersParent, toggleOuterNumbersButtonText, " Outer Numbers");
        ToggleTextButton(false, innerNumbersParent, toggleInnerNumbersButtonText, " Inner Numbers");
        ToggleTextButton(false, middleNumber, toggleMiddleNumberButtonText, " Middle Number");
    }

    public void RandomiseInnerNumbers()
    {
        ShuffleArray(randomInnerNumbers);

        UpdateNumbers();
    }
    
    public void RandomiseMiddleNumber()
    {
        activeMiddleNumber = GetRandomMiddleNumber();

        UpdateNumbers();
    }

    private int GetRandomMiddleNumber()
    {
        int temp = randomNumbers[Random.Range(0, randomNumbers.Length)];
        if (temp == activeMiddleNumber)
        {
            GetRandomMiddleNumber();
        }

        return temp;
    }
}
